#include <stdio.h>
#define TABLESIZE 100
char hashTableLinear[TABLESIZE], hashTableQuad[TABLESIZE];

int hashFunc(char keyVal) {
	int index = ( keyVal % 29) + 1;
	return index;
}

void main() {
	int k;
	for( k=0; k<TABLESIZE; k ++ ) {
		hashTableLinear[k] = -1;
		hashTableQuad[k] = -1 ;
	}

  int keySize, index = 0, controlVar, controlVarJ, i, flag = 0;
	char keyVal[75];
	printf("Enter the key values: ");
	scanf("%s", keyVal);
	for(keySize = 0; keyVal[keySize] != '\0'; keySize ++);

	//Linear Probing
	for( controlVar = 0; controlVar<keySize; controlVar ++ ) {
		index = hashFunc( keyVal[controlVar] );
		if( hashTableLinear[index] < 0 ) {
			hashTableLinear[index] = keyVal[controlVar];
		}
		else {
			controlVarJ = index + 1;
			while( controlVarJ != index ) {
				if( hashTableLinear[controlVarJ] < 0 ) {
					hashTableLinear[controlVarJ] = keyVal[controlVar];
					break;
				}

				if( controlVarJ == TABLESIZE-1 ) {
					controlVarJ = 0;
				}
				else {
					controlVarJ ++;
				}
			}
		}
	}

	//Quadratic Probing
	for( controlVar = 0; controlVar < keySize; controlVar++ ) {
		i = 1;
		index = hashFunc( keyVal[controlVar] );
		if( hashTableQuad[index] < 0 ) {
			hashTableQuad[index] = keyVal[controlVar];
		}
		else {
			controlVarJ = index + 1;
			while( controlVarJ != index ) {
				if ( ( flag == 1 ) && ( controlVarJ >= index ) ) {
					break;
				}
				if( hashTableQuad[controlVarJ] < 0 ) {
					hashTableQuad[controlVarJ] = keyVal[controlVar];
					break;
				}

				controlVarJ = controlVarJ + ( ( 2 * i ) + 1 );
				i++;


				if (controlVarJ >= TABLESIZE ) {
					controlVarJ = ( controlVarJ % TABLESIZE );
					flag = 1;
				}
			}
		}
	}
	printf("Index\tLinear\tQuadratic\n" );
	for( i = 0; i < TABLESIZE; i ++ ) {
		printf("%d\t", i );
		if(hashTableLinear[i] == -1 ) {
			printf("-\t");
		}
		else {
			printf("%c\t", hashTableLinear[i] );
		}
		if( hashTableQuad[i] == -1 ) {
			printf("-\n");
		}
		else {
			printf("%c\n", hashTableQuad[i] );
		}
	}
}
